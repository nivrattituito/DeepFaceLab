
from core.leras import nn
nn.initialize_main_env()
import os
import sys
import time
import argparse

from core import pathex
from core import osex
from pathlib import Path
from core.interact import interact as io

if sys.version_info[0] < 3 or (sys.version_info[0] == 3 and sys.version_info[1] < 6):
    raise Exception("This program requires at least Python 3.6")


##
# note --
# program will ask value if default None is passed to argument.
# ex: if output_debug=None set then it will ask value at runtime

def process_videoed_extract_video(input_file, output_dir, output_ext="png", fps=0):
    """
    Extract images from video file
    """
    osex.set_process_lowest_prio()
    from mainscripts import VideoEd
    VideoEd.extract_video (input_file, output_dir, output_ext, fps)
    return True

def process_extract(
    input_dir, output_dir, detector="s3fd",  output_debug=False, face_type="full_face", max_faces_from_image=0, image_size=512, 
    jpeg_quality=100, manual_fix=False, manual_output_debug_fix=False, manual_window_size=1368, cpu_only=False, force_gpu_idxs="0", 
    ):
    # osex.set_process_lowest_prio()
    from mainscripts import Extractor
    Extractor.main( 
        detector                = detector,
        input_path              = Path(input_dir),
        output_path             = Path(output_dir),
        output_debug            = output_debug,
        manual_fix              = manual_fix,
        manual_output_debug_fix = manual_output_debug_fix,
        manual_window_size      = manual_window_size,
        face_type               = face_type,
        max_faces_from_image    = max_faces_from_image,
        image_size              = image_size,
        jpeg_quality            = jpeg_quality,
        cpu_only                = cpu_only,
        force_gpu_idxs          = [ int(x) for x in force_gpu_idxs.split(',') ] if force_gpu_idxs is not None else None,
    )
    return True

def process_train(
    training_data_src_dir, training_data_dst_dir, model_dir, model_name=None,
    pretraining_data_dir=None, pretrained_model_dir=None,
    no_preview=False, force_model_name=None, force_gpu_idxs=None,
    cpu_only=False, silent_start=None, execute_program=[], debug=False
    ):
    """
    Trainer - 
    """
    osex.set_process_lowest_prio()

    kwargs = {
        'model_class_name'         : model_name,
        'saved_models_path'        : Path(model_dir),
        'training_data_src_path'   : Path(training_data_src_dir),
        'training_data_dst_path'   : Path(training_data_dst_dir),
        'pretraining_data_path'    : Path(pretraining_data_dir) if pretraining_data_dir is not None else None,
        'pretrained_model_path'    : Path(pretrained_model_dir) if pretrained_model_dir is not None else None,
        'no_preview'               : no_preview,
        'force_model_name'         : force_model_name,
        'force_gpu_idxs'           : [ int(x) for x in force_gpu_idxs.split(',') ] if force_gpu_idxs is not None else None,
        'cpu_only'                 : cpu_only,
        'silent_start'             : silent_start,
        'execute_programs'         : [ [int(x[0]), x[1] ] for x in execute_program ],
        'debug'                    : debug,
    }
    from mainscripts import Trainer
    Trainer.main(**kwargs)

def process_merge(
    input_dir, output_dir, output_mask_dir,
    aligned_dir=None, model_dir="model/", model_name="SAEHD", force_model_name=None,
    cpu_only=False, force_gpu_idxs="0",
    ):
    """
    Merger
    """
    osex.set_process_lowest_prio()
    from mainscripts import Merger
    Merger.main( 
        model_class_name       = model_name,
        saved_models_path      = Path(model_dir),
        force_model_name       = force_model_name,
        input_path             = Path(input_dir),
        output_path            = Path(output_dir),
        output_mask_path       = Path(output_mask_dir),
        aligned_path           = Path(aligned_dir) if aligned_dir is not None else None,
        force_gpu_idxs         = force_gpu_idxs,
        cpu_only               = cpu_only
    )

def process_videoed_video_from_sequence(
    input_dir, output_file, reference_file,
    ext='png', fps=0, bitrate=0, 
    include_audio=False, lossless=False,
    ):
    """
    video-from-sequence
    """
    osex.set_process_lowest_prio()
    from mainscripts import VideoEd
    VideoEd.video_from_sequence (
        input_dir      = input_dir,
        output_file    = output_file,
        reference_file = reference_file,
        ext      = ext,
        fps      = fps,
        bitrate  = bitrate,
        include_audio = include_audio,
        lossless = lossless
    )
    return True


def main():
    # video_processing_dir = "/home/nivratti/Desktop/utngl-app/face-swapping-ageing/media/videos/VDO2019HD_0217/"
    video_processing_dir = "/home/nivratti/Desktop/utngl-app/face-swapping-ageing/media/videos/Woman-Wearing-White-Shirt-Waving-Goodbye/"
    
    # # test video extraction
    # video_input_file = os.path.join(video_processing_dir, "data_dst.mov")
    # video_frames_output_dir = os.path.join(video_processing_dir, "data_dst/frames")
    # os.makedirs(video_frames_output_dir, exist_ok=True)
    # process_videoed_extract_video(video_input_file, video_frames_output_dir)

    # # test face extraction
    # face_extraction_input_dir = os.path.join(video_processing_dir, "data_dst/frames")
    # face_extraction_output_dir = os.path.join(video_processing_dir, "data_dst/aligned")
    # os.makedirs(face_extraction_output_dir, exist_ok=True)
    # # whole_face, full_face
    # process_extract(
    #     face_extraction_input_dir,
    #     face_extraction_output_dir,
    #     face_type="full_face"
    # )

    # face_extraction_input_dir = os.path.join(video_processing_dir, "data_src/frames/")
    # face_extraction_output_dir = os.path.join(video_processing_dir, "data_src/aligned/")
    # os.makedirs(face_extraction_output_dir, exist_ok=True)
    # # whole_face, full_face
    # process_extract(
    #     face_extraction_input_dir,
    #     face_extraction_output_dir,
    #     face_type="full_face"
    # )
    
    # # ##------------------------------
    # # ## Training
    # # training_data_src_dir = os.path.join(video_processing_dir, "data_src/aligned/")
    # # training_data_dst_dir = os.path.join(video_processing_dir, "data_dst/aligned/")
    # # model_dir = os.path.join(video_processing_dir, "model")
    # # os.makedirs(model_dir, exist_ok=True)

    # # process_train(
    # #     training_data_src_dir, training_data_dst_dir, model_dir, model_name="SAEHD",
    # #     no_preview=False, force_model_name="younger", force_gpu_idxs="0",
    # #     cpu_only=False, silent_start=False, execute_program=[], debug=False
    # # )

    # using pretrained model
    # it will improve speed and quality
    training_data_src_dir = os.path.join(video_processing_dir, "data_src/aligned/")
    training_data_dst_dir = os.path.join(video_processing_dir, "data_dst/aligned/")
    model_dir = os.path.join(video_processing_dir, "model")

    script_dir = os.path.dirname(os.path.abspath(__file__))
    pretraining_data_dir = os.path.join(script_dir, "pretrain")
    if not os.path.exists(pretraining_data_dir):
        pretraining_data_dir = os.path.join(script_dir, "Deepfacelab/pretrain")
        if not os.path.exists(pretraining_data_dir):
            print("Error... Pretrained data dir not found...")

    pretrained_model_dir = model_dir
    os.makedirs(model_dir, exist_ok=True)

    process_train(
        training_data_src_dir, training_data_dst_dir, model_dir, model_name="SAEHD",
        no_preview=False, force_model_name=None, force_gpu_idxs="0",
        pretraining_data_dir=pretraining_data_dir, pretrained_model_dir=pretrained_model_dir,
        cpu_only=False, silent_start=False, execute_program=[], debug=False
    )

    # ##------------------------------
    # ## Merger
    # input_dir = os.path.join(video_processing_dir, "data_dst/frames")
    # output_dir = os.path.join(video_processing_dir, "data_dst/merged")
    # output_mask_dir = os.path.join(video_processing_dir, "data_dst/merged_mask")
    # aligned_dir = os.path.join(video_processing_dir, "data_dst/aligned/")
    # model_dir = os.path.join(video_processing_dir, "model")
    # process_merge(
    #     input_dir, output_dir, output_mask_dir,
    #     aligned_dir, model_dir, model_name="SAEHD",
    #     cpu_only=False, force_gpu_idxs=None,
    # )
    # pass

    # ## video from sequence
    # input_dir = os.path.join(video_processing_dir, "data_dst/merged")
    # output_file = os.path.join(video_processing_dir, "result.mp4")
    # reference_file = os.path.join(video_processing_dir, "data_dst.mp4")

    # process_videoed_video_from_sequence(
    #     input_dir, output_file, reference_file,
    #     ext='png', fps=0, bitrate=0, 
    #     include_audio=False, lossless=False,
    # )


if __name__ == "__main__":
    # Fix for linux
    import multiprocessing
    multiprocessing.set_start_method("spawn")

    main()